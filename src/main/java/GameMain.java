import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

interface Game {
    void initialize();
    void update();
    void render();
}

class GameApplication {
    public static void boot(Game game) {
        GLFWErrorCallback.createPrint(System.err).set();
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        long windowHandle = glfwCreateWindow(640, 480, "Hello Polygon", NULL, NULL);
        if (windowHandle == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }
        glfwMakeContextCurrent(windowHandle);
        glfwSwapInterval(1);
        glfwShowWindow(windowHandle);

        GL.createCapabilities();
        game.initialize();
        while (!glfwWindowShouldClose(windowHandle)) {
            game.update();
            game.render();
            glfwSwapBuffers(windowHandle);
            glfwPollEvents();
        }

        glfwFreeCallbacks(windowHandle);
        glfwDestroyWindow(windowHandle);
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }
}

public class GameMain implements Game {
    @Override
    public void initialize() {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void update() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        float angle = 360 * (float)(glfwGetTime() % 1);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.5f, 0.0f);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f(-0.5f, -0.5f, 0.0f);
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(0.5f, -0.5f, 0.0f);
        glEnd();
    }

    public static void main(String[] args) {
        GameApplication.boot(new GameMain());
    }
}